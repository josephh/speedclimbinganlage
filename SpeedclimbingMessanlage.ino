#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Adafruit_GFX.h"
#include "ESP32RGBmatrixPanel.h"
//Verwendet diese Library
//https://github.com/VGottselig/ESP32-RGB-Matrix-Display

//Kompiliert mit Firebeetle 32


//Andere Seite des Kabels:
//G1	R1 |  16    17
//GND	B1 |  GND   4
//G2	R2 |  2     0
//GND	B2 |  GND   15
//B		A  |  19    21
//D		C  |  5     18
//LAT	CLK|  3     22
//GND	OE |  GND   23


//ESP32RGBmatrixPanel matrix(23, 22, 03, 17, 16, 04, 00, 02, 15, 21, 19, 18, 5); //Flexible connection

//Default connection
//uint8 OE = 23;
//uint8 CLK = 22;
//uint8 LAT = 03;
//uint8 CH_A = 21;
//uint8 CH_B = 19;
//uint8 CH_C = 18;
//uint8 CH_D = 5;
//uint8 R1 = 17;
//uint8 G1 = 16;
//uint8 BL1 = 4;
//uint8 R2 = 0;
//uint8 G2 = 2;
//uint8 BL2 = 15;

#define SPKR 25
#define READY 0
#define COUNTDOWN 1
#define BOTH_CLIMBING 2
#define CLIMB_1_FINISH 3 //Climber 1 finished and climber 2 is still climbing
#define CLIMB_2_FINISH 4 //Climber 2 finished and climber 1 is still climbing
#define FINISHED 5 //Both or one climber finished
#define BLACK 100

 /* create a hardware timer */
 hw_timer_t* displayUpdateTimer = NULL;
 hw_timer_t* starterTimer = NULL;
 
 ESP32RGBmatrixPanel matrix;
long DRAM_ATTR start;
long DRAM_ATTR stopClimber1;
long DRAM_ATTR stopClimber2;
boolean DRAM_ATTR climber1 = false;
boolean DRAM_ATTR climber2 = false;
long DRAM_ATTR countdown;
long DRAM_ATTR state;
long DRAM_ATTR button2Debounce;
long DRAM_ATTR button1Debounce;
long DRAM_ATTR button3Debounce;


//Legt den ESP schlafen, wakeup funktioniert nur so semi...
void goToSleep(){
  timerDetachInterrupt(displayUpdateTimer);
  matrix.black();
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_34, HIGH);
  esp_deep_sleep_start();
}

 //runs faster then default void loop(). why? runs on other core?
 //Diese methode wird ziemlich schnell immer wieder aufgerufen.
 void loop2_task(void *pvParameter)
 {
	 while (true)
	 {  
      if(state != READY && state != COUNTDOWN && millis() - start > 1000){
  	    if(millis() - stopClimber1 < 700 || millis() - stopClimber2 < 700){
          ledcAttachPin(25,0);
  	    }else{
          ledcDetachPin(25);
  	    }
      }
      if(state == READY){
        showCountdown(5000);
      }
      if(state == COUNTDOWN){
            long millisTillStart = 5000-(millis()-countdown);
            showCountdown(millisTillStart);
            if(millisTillStart < 5000 && millisTillStart > 4750 ||
                millisTillStart < 4000 && millisTillStart > 3750 ||
                millisTillStart < 3000 && millisTillStart > 2750 ||
                millisTillStart < 2000 && millisTillStart > 1750 ||
                millisTillStart < 1000 && millisTillStart > 750){
              ledcAttachPin(25,0);
            }else{
              ledcDetachPin(25);
            }
      }
      if(state == FINISHED){
            if(stopClimber1 != 0 && stopClimber2 != 0){
              showResult(stopClimber1-start,stopClimber2-start);
            }else{
              if(stopClimber1 != 0){
                showOneResult(stopClimber1-start);
              }else{
                showOneResult(stopClimber2-start);
              }
            }
      }
      if(state == BOTH_CLIMBING){
            showTimeDuringClimb(millis()-start);
            if(millis() - start < 800){
              ledcAttachPin(25,0);
            }else{
              ledcDetachPin(25);
            }
      }
      if(state == CLIMB_1_FINISH){
            showResult(stopClimber1-start,millis()-start);
      }
      if(state == CLIMB_2_FINISH){
            showResult(millis()-start,stopClimber2-start);
      }
      if(state == BLACK){
            matrix.black();
      }
      long timeSinceAction = 0;
      if((state == READY || state == FINISHED) && (timeSinceAction = millis() - max(stopClimber1 , max( stopClimber2, button3Debounce))) > 30000){
        goToSleep();
      }else{
        if((state == BOTH_CLIMBING || state == CLIMB_1_FINISH || state == CLIMB_2_FINISH) && (timeSinceAction = millis()-max(max(start, stopClimber1) , max(stopClimber2, button3Debounce))) > 240000){
          goToSleep();
        }
      }
    delay(10);
	 }
 }




 void IRAM_ATTR onDisplayUpdate() {
	 matrix.update();
 }
 


void IRAM_ATTR startTime(){
  timerDetachInterrupt(starterTimer);
  timerAlarmDisable(starterTimer);
  Serial.println("start");
  Serial.print("climber1: ");
  Serial.println(climber1);
  Serial.print("climber2: ");
  Serial.println(climber2);
  stopClimber1 = 0;
  stopClimber2 = 0;
  start = millis();
  state = BOTH_CLIMBING;
  ledcWriteTone(0,700);
}
void IRAM_ATTR finish(){
      state = FINISHED;
      climber1 = false;
      climber2 = false;
      Serial.println("Finished");
      Serial.print("climber1: ");
      Serial.print(stopClimber1);
      Serial.print(" -> ");
      Serial.println(stopClimber1-start);
      Serial.print("climber2: ");
      Serial.print(stopClimber2);
      Serial.print(" -> ");
      Serial.println(stopClimber2-start);
}
void IRAM_ATTR startCountdown(){
  state = COUNTDOWN;
  countdown = millis();
  starterTimer = timerBegin(1, 80, true);
  timerAttachInterrupt(starterTimer, &startTime, true);
  timerAlarmWrite(starterTimer, 5000000, true);
  timerAlarmEnable(starterTimer);
}

void IRAM_ATTR button1Pressed(){
  if(millis() - button1Debounce < 600){
    return;
  }
  button1Debounce = millis();
  if(state == READY){
    climber1 = !climber1;
  }
}

void IRAM_ATTR button2Pressed(){
  if(millis() - button2Debounce < 600){
    return;
  }
  button2Debounce = millis();
  if(state == READY){
    climber2 = !climber2;
  }
}

void IRAM_ATTR buzzer1Pressed(){
  if(stopClimber1 == 0 && climber1){
    stopClimber1 = millis();
    if(state != CLIMB_2_FINISH && climber2){
      state = CLIMB_1_FINISH;
    }else{
      finish();
    }
    
  }
}

void IRAM_ATTR buzzer2Pressed(){
  if(stopClimber2 == 0 && climber2){
    stopClimber2 = millis();
    if(state != CLIMB_1_FINISH && climber1){
      state = CLIMB_2_FINISH;
    }else{
      finish();
    }
  }
}

void IRAM_ATTR buttonRSTPressed(){
  if(millis() - button3Debounce < 600){
    return;
  }
  button3Debounce = millis();
  ledcWriteTone(0, 350);
  if(state != READY){
    stopClimber1 = 0;
    stopClimber2 = 0; 
    climber1 = false;
    climber2 = false;
    state = READY;
    timerDetachInterrupt(starterTimer);
    timerAlarmDisable(starterTimer);
    ledcDetachPin(25);
  }else{
      if(climber1 || climber2){
        startCountdown();
      }else{
        goToSleep();
      }
  }
}
void setup() {
  pinMode(32, INPUT);   //Startknopf Kletterer 1
  pinMode(33, INPUT);   //Startknopf Kletterer 2
  pinMode(34, INPUT);   //Reset Knopf
  pinMode(13, INPUT);   //Endbuzzer Kletterer 1
  pinMode(14, INPUT);   //Endbuzzer Kletterer 2
  pinMode(25, OUTPUT);  //Lautsprecher
	matrix.setBrightness(10);

//Set up tone
 ledcSetup(0, 200, 8); //ledcSetup(channel(immer 0), frequency, resolution bits)
 ledcWrite(0, 255);    // ???
 ledcWriteTone(0, 350);//ledcWriteTone(channel (immer 0), frequency)

  while(digitalRead(34) == HIGH){}



 /*
  xTaskCreate(
    &function,    // Function that should be called
    "Toggle LED",   // Name of the task (for debugging)
    1000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );
  
  */

	xTaskCreate(&loop2_task, "loop2_task", 2048, NULL, 5, NULL);
  
	/*
   * As input, this function receives the number of the timer we want to use (from 0 to 3, since we have 4 hardware timers), the value of the prescaler and a flag indicating if the counter should count up (true) or down (false).
   * timerBegin(timerId (von 0 bis 3), prescaler (s.u.), count up or down)
von https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
"Regarding the prescaler, we have said in the introductory section that typically the frequency of the base signal used by the ESP32 counters is 80 MHz 
(this is true for the FireBeetle board). This value is equal to 80 000 000 Hz, which means means the signal would make the timer counter increment 80 000 000 times per second.
Although we could make the calculations with this value to set the counter number for generating the interrupt, we will take advantage of the prescaler to simplify it. 
Thus, if we divide this value by 80 (using 80 as the prescaler value), we will get a signal with a 1 MHz frequency that will increment the timer counter 1 000 000 times per second."

Das heißt bei Wert 80 als prescaler wird jede Microsekunde der Counter erhöht. Bei timerAlarmWrite kann als als zweiter Eingabeparameter die frequenz in microsekunden angegeben werden.
	 */
	displayUpdateTimer = timerBegin(0, 80, true);

  state = READY;
  
	/* Attach onTimer function to our timer */
	timerAttachInterrupt(displayUpdateTimer, &onDisplayUpdate, true);
	timerAlarmWrite(displayUpdateTimer, 2, true);
	timerAlarmEnable(displayUpdateTimer);
//Ab jetzt wird onDisplayUpdate alle 2 microsekunden ausgeführt -> 500000 Mal in der Sekunde (warum so oft???)
 
  attachInterrupt(13, &buzzer1Pressed, RISING);
  attachInterrupt(14, &buzzer2Pressed, RISING);
  attachInterrupt(32, &button1Pressed, RISING);
  attachInterrupt(33, &button2Pressed, RISING);
  attachInterrupt(34, &buttonRSTPressed, FALLING);


//Textfarbe Text color einstellen:
   byte br = 255;
   auto white = ESP32RGBmatrixPanel::AdafruitColor(br, br, br);
   matrix.setTextWrap(false);
   matrix.setTextColor(white);
}




void loop() {
  //Die loop bleibt leer, da alles im Timer passier
}
void showReady(){
   matrix.black();
   matrix.setTextSize(1);
   matrix.setCursor(1, 1);
   matrix.print("bereit!");
   matrix.setTextSize(1);
   matrix.setCursor(1, 15);
   matrix.print("zum Start");
   matrix.setCursor(1, 23);
   matrix.print("druecken");
}
void showCountdown(long timeRemaining){   
   matrix.black();
   matrix.setTextSize(1);
   matrix.setCursor(1, 1);
   if(state == READY){
      if(climber1 || climber2){
        matrix.print("  bereit  ");
      }else{
        matrix.print("Bahn ausw.");
      }
   }else{
        matrix.print(" start in ");
   }
   matrix.setTextSize(2);
   matrix.setCursor(15, 15);
   matrix.print(timeRemaining/1000.0,1);

   byte br = 255;
   auto white = ESP32RGBmatrixPanel::AdafruitColor(br, br, br);

   if(climber1)
    matrix.fillRect(0, 21, 5, 10, white);
   if(climber2)
    matrix.fillRect(59, 21, 5, 10, white);
}
void showTimeDuringClimb(long timeRunning){
   
   matrix.black();
   if(timeRunning/1000 >= 100){
      matrix.setTextSize(1);
      matrix.setCursor(11, 13);
   }else{
     matrix.setTextSize(2);
     matrix.setCursor(1, 9);
   }
   if(timeRunning/1000 < 10){
    matrix.print(" ");
   }
   matrix.print(timeRunning/1000.0,2);
}
void showOneResult(long result){
   matrix.black();
   if(result/1000 > 100){
      matrix.setTextSize(1);
      matrix.setCursor(11, 13);
   }else{
     matrix.setTextSize(2);
     matrix.setCursor(1, 9);
   }
   if(result/1000 < 10){
    matrix.print(" ");
   }
   matrix.print(result/1000.0,2);
}
void showResult(long climber1Result, long climber2Result){
   
   matrix.black();
   matrix.setTextSize(1);
   matrix.setCursor(3, 3);
   matrix.print("<");
   matrix.print(climber1Result/1000.0, 2);
   matrix.setCursor(14, 21);
   if(climber2Result/1000 < 100){
    matrix.print(" ");
   }
   matrix.print(climber2Result/1000.0, 2);
   matrix.print(">");
}
